package net.nesley.eyassum.model;

import net.nesley.eyassum.dao.entity.User;

public class TokenRoleUserDto {

    private String token;
    private String userName;
    private String role;



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}


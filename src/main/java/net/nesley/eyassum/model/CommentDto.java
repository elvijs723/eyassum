package net.nesley.eyassum.model;

import net.nesley.eyassum.dao.entity.Comment;

import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CommentDto {

    private Long articleId;
    @NotBlank
    @Lob
    @Size(min=2, max=160)
    private String content;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

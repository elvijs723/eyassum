package net.nesley.eyassum.service.serviceImpl;

import net.nesley.eyassum.dao.UserRepository;
import net.nesley.eyassum.dao.entity.User;
import net.nesley.eyassum.model.Message;
import net.nesley.eyassum.model.Role;
import net.nesley.eyassum.model.UserDto;
import net.nesley.eyassum.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.Arrays;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(s);
        if(s==null) {
            throw new CustomException("Incorrect username or password",HttpStatus.NOT_FOUND);
        }

        return org.springframework.security.core.userdetails.User
                .withUsername(s)
                .password(user.getPassword())
                .authorities(user.getRoles())
                .build();
    }


    public String signin(String username, String password){
        try{
            Authentication auth =authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
                    (username,password));
            SecurityContextHolder.getContext().setAuthentication(auth);

            User user = userRepository.findByUsername(username);
            if (user==null) {
                throw new CustomException("Username not found",HttpStatus.NOT_FOUND);
            }
            return jwtTokenProvider.createToken(username,user.getRoles());
        }
        catch(AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    public Message save(User user){
//        User newUser;

//            if(userRepository.exists(user.getUsername()).getUsername()==null));


//            throw new CustomException("This username is already in use",HttpStatus.CONFLICT);

//        User user = new User();
//        user.setUsername(userDto.getUsername());
//        user.setPassword(userDto.getPassword());
//        user.setEmail(userDto.getEmail());
//        user.setInfo(userDto.getInfo());
        user.setRoles(Arrays.asList(Role.ROLE_CLIENT));
        try {
            userRepository.save(user);
        }
        catch (Exception e){
            throw new CustomException("This username is already in use", HttpStatus.CONFLICT);
        }
        return new Message("Registration succesfull.");
    }

    public User update(User user){
        return userRepository.save(user);
    }

//    public User saveUser(UserDto userDto){
//
//        // 1.
//        User user = new User();
//        user.setUsername(userDto.getUsername());
//        user.setPassword(userDto.getPassword());
//        user.setEmail(userDto.getEmail());
//
//        user.setRoles(Arrays.asList(Role.ROLE_CLIENT));
//        return userRepository.save(user);
//    }



//    UsernamePasswordAuthenticationToken usernamePasswordAuth = getAuthenticationToken(request);
//        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuth);
//        chain.doFilter(request,response);
//
//
//    @Override
//    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
//        UserDto userDto = findByUsername(s);
//        if(userDto==null){
//            throw new UsernameNotFoundException("Invalid username or password");
//        }
//        return new User(userDto.getUsername(),userDto.getPassword(), AuthorityUtils.createAuthorityList("ROLE_USER"));
//    }


    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public Message delete(Long id){
        userRepository.deleteById(id);
        return new Message("Deleted");
    }

}

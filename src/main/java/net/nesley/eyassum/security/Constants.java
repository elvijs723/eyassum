package net.nesley.eyassum.security;

public class Constants {

    public final static String SECRET = "secre";
    public final static String TOKEN_PREFIX = "Bearer ";
    public final static String HEADER_STRING = "Authorization";
    public final static long EXPIRATION_TIME = 874_000_000L;
}

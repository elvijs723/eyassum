package net.nesley.eyassum.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.nesley.eyassum.model.Role;
import net.nesley.eyassum.service.serviceImpl.CustomException;
import net.nesley.eyassum.service.serviceImpl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import static net.nesley.eyassum.security.Constants.EXPIRATION_TIME;
import static net.nesley.eyassum.security.Constants.HEADER_STRING;
import static net.nesley.eyassum.security.Constants.SECRET;
import static net.nesley.eyassum.security.Constants.TOKEN_PREFIX;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {

    private String secretKey;

    @Autowired
    private UserService userService;

    @PostConstruct
    protected void init(){
        secretKey= Base64.getEncoder().encodeToString(SECRET.getBytes());
    }

    public String createToken(String username, List<Role> roles) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("auth", roles.stream()
                .map(r->new SimpleGrantedAuthority(r.getAuthority()))
                .filter(Objects::nonNull).collect(Collectors.toList()));

        ZonedDateTime expirationTimeUTC = ZonedDateTime.now(ZoneOffset.UTC)
                .plus(EXPIRATION_TIME, ChronoUnit.MILLIS);

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date.from(expirationTimeUTC.toInstant()))
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public String resolveToken(HttpServletRequest req){
//        String bearer = req.getHeader("Authorization");
        String token = req.getHeader(HEADER_STRING);
        return (token!=null && token.startsWith(TOKEN_PREFIX))?
                token.replace(TOKEN_PREFIX,""):null;
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public Authentication attemptTokenAuthentication(String token){
        String username = Jwts.parser().setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();

        UserDetails userDetails = userService.loadUserByUsername(username);

        return username!=null ? new UsernamePasswordAuthenticationToken
                (userDetails,null, userDetails.getAuthorities()):null;
    }
}
//    y(s.getAuthority())).filter(Objects::nonNull).collect(Collectors.toList()));
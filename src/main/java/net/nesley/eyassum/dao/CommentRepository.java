package net.nesley.eyassum.dao;

import net.nesley.eyassum.dao.entity.Article;
import net.nesley.eyassum.dao.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findByArticleId(Long articleId, Pageable pageable);
    Page<Comment> getByArticleIdDateDesc(Article article, Pageable pageable);
//    Page<Comment> findByArticleIdOrderByIdAsc(Long articleId, Pageable pageable);
//    Page<Comment> findAllByArticleIdOrderByCreatedAt(Long articleId, Pageable pageable);
}

package net.nesley.eyassum.dao;

import net.nesley.eyassum.dao.entity.Article;
import net.nesley.eyassum.dao.entity.Comment;
import net.nesley.eyassum.dao.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article,Long> {
//    Page<Comment> findByUserId(Long userId, Pageable pageable);

    Page<Article> findByUserIdOrderByIdDesc(Long userId, Pageable pageable);
    Page<Article> findAllByOrderByIdDesc(Pageable pageable);
    Page<Article> findById(Pageable pageable);
    Page<Article> getByUserId(User user, Pageable pageable);
}

package net.nesley.eyassum.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;


import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
//@NamedQuery(name = "Comment.findAllByArticleIdOrderByCreatedAt",
//        query = "select c from Comment c where c.article = ?1 " +
//                "order by c.createdAt DESC")
@Entity
@Table(name="comments")
@javax.persistence.NamedQuery(name="Comment.getByArticleIdDateDesc", query="Select c from Comment c where c.article = ?1 order by c.createdAt DESC")
public class Comment extends TimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Lob
    @Size(min=2, max=160)
    private String content;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="article_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Article article;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="user_id", nullable = false)
    @OnDelete(action=OnDeleteAction.CASCADE)
    @JsonIgnore
    private User user;


    private String author;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}

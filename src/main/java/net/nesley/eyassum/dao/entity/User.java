package net.nesley.eyassum.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.nesley.eyassum.model.Role;


import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(name="username", length=30, unique=true)
    @Size(min=3,max=30,message="Username has to be of length {min} to {max}")
    private String username;

    @NotBlank
//    @JsonIgnore
    @Size(min=6,max=30,message="Password has to be of length {min} to {max}")
    private String password;

    @NotBlank
    @Column(name="email", length=30, unique=true)
    @Size(min=3,max=55,message="Email has to be of length {min} to {max}")
    private String email;

    @Lob
    @Column(name="info", length=150)
    private String info;


    @ElementCollection(fetch = FetchType.EAGER)
    List<Role> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}

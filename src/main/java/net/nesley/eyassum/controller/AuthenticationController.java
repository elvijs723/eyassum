package net.nesley.eyassum.controller;


import net.nesley.eyassum.dao.entity.User;
import net.nesley.eyassum.model.LoginUser;
import net.nesley.eyassum.model.Message;
import net.nesley.eyassum.model.UserDto;
import net.nesley.eyassum.model.TokenRoleUserDto;
import net.nesley.eyassum.service.serviceImpl.CustomException;
import net.nesley.eyassum.service.serviceImpl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;
import javax.validation.Valid;

@CrossOrigin(origins="*", maxAge=3600)
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity<TokenRoleUserDto> login(@RequestBody LoginUser loginUser) throws AuthenticationException{
        final String token = userService.signin(loginUser.getUsername(),loginUser.getPassword());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String role = auth.getAuthorities().toString();

        TokenRoleUserDto tokenUser = new TokenRoleUserDto();
        tokenUser.setRole(role);
        tokenUser.setUserName(loginUser.getUsername());
        tokenUser.setToken(token);

        return ResponseEntity.ok(tokenUser);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody User user, Errors errors) throws AuthenticationException{
        if(errors.hasFieldErrors()){
            throw new CustomException(errors.getFieldError().getDefaultMessage(), HttpStatus.LENGTH_REQUIRED);
        }
//        if(errors.hasErrors())
//        User newUser = userService.save(user);
        return ResponseEntity.ok(userService.save(user));
    }

    public static String cleanRole(String r){
        return "";
    }
}

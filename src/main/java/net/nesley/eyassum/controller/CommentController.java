package net.nesley.eyassum.controller;

import net.nesley.eyassum.dao.ArticleRepository;
import net.nesley.eyassum.dao.CommentRepository;
import net.nesley.eyassum.dao.entity.Article;
import net.nesley.eyassum.dao.entity.Comment;
import net.nesley.eyassum.dao.entity.User;
import net.nesley.eyassum.model.CommentDto;
import net.nesley.eyassum.model.Message;
import net.nesley.eyassum.service.serviceImpl.CustomException;
import net.nesley.eyassum.service.serviceImpl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@CrossOrigin(origins="*", maxAge=3600)
@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserService userService;

    @GetMapping("/{id}/{page}")
    public Page<Comment> getAllComments(@PathVariable(value="id") Long id,
            @PathVariable(value="page") int page, Pageable pageable){
        Article article = articleRepository.getOne(id);
        return commentRepository.getByArticleIdDateDesc(article,new PageRequest(page, 10));
//        return commentRepository.findByArticleId(id,new PageRequest(page,10));
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> commitComment(@Valid @RequestBody CommentDto commentDto, Errors errors, Principal principal) {
        if(errors.hasFieldErrors()){
            throw new CustomException(errors.getFieldError().getDefaultMessage(), HttpStatus.CONFLICT);
        }
    String currentUser= principal.getName();
    Article article = articleRepository.getOne(commentDto.getArticleId());
    User user = userService.findByUsername(currentUser);
    Comment comment = new Comment();
    comment.setUser(user);
    comment.setAuthor(currentUser);
    comment.setArticle(article);
    comment.setContent(commentDto.getContent());
    commentRepository.save(comment);
    return ResponseEntity.ok(new Message("Commit submitted"));
    }
//    @PostMapping("/{articleId}/comment")
//    public Comment createComment(@PathVariable (value="articleId")Long articleId, @Valid @RequestBody Comment comment) {
//        Article article = articleRepository.findById(articleId).get();
//        comment.setArticle(article);
//        return commentRepository.save(comment);
//
//    }
}

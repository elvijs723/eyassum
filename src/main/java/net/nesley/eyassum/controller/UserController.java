package net.nesley.eyassum.controller;

import net.nesley.eyassum.dao.entity.User;
import net.nesley.eyassum.model.LoginUser;
import net.nesley.eyassum.model.Message;
import net.nesley.eyassum.model.UserDto;
import net.nesley.eyassum.service.serviceImpl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@CrossOrigin(origins="*", maxAge=3600)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/{username}")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserDto getUserByName(@PathVariable(value="username") String username){

        System.out.println(SecurityContextHolder.getContext().getAuthentication());

        User user = userService.findByUsername(username);
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setInfo(user.getInfo());
//        userDto.setRoles(user.getRoles());
        return userDto;
    }

//    @PostMapping
//    public void register(@RequestBody UserDto userDto) {
//        userService.save(userDto);
//    }


//    @GetMapping
//    @PreAuthorize("hasRole('ROLE_CLIENT')")
//    public ResponseEntity<?> enterOffice2(){
//        LoginUser u = new LoginUser("floor1","office2");
//        return new ResponseEntity<>(u, HttpStatus.OK);
//    }
    @ResponseBody
    @PutMapping
    public ResponseEntity<?> updateInfo(@RequestBody String text, Principal principal){
        String userName = principal.getName();
        User user = userService.findByUsername(userName);
        User user2 = user;
        user.setInfo(text);
        userService.update(user);
        return ResponseEntity.ok(new Message("Updated successfully"));
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteUser(@PathVariable(value="id") Long id, Principal principal) {


        return ResponseEntity.ok( userService.delete(id));
    }

}

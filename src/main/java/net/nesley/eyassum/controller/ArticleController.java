package net.nesley.eyassum.controller;

import net.nesley.eyassum.dao.ArticleRepository;
import net.nesley.eyassum.dao.entity.Article;
import net.nesley.eyassum.dao.entity.User;
import net.nesley.eyassum.model.ArticleDto;
import net.nesley.eyassum.model.Message;
import net.nesley.eyassum.service.serviceImpl.CustomException;
import net.nesley.eyassum.service.serviceImpl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@CrossOrigin(origins="*", maxAge=3600)
@RestController
@RequestMapping({"/article"})
public class ArticleController {
    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserService userService;
//    @GetMapping()
//    public Page<Article> getAllPosts(Pageable pageable){
//        return postRepository.findAll(pageable);
//    }
    @GetMapping("/{id}")
    public Article getArticle(@PathVariable(value="id")Long id ){
//    return articleRepository.findById(new PageRequest(page,10));
        return articleRepository.findById(id).get();
    }
    @GetMapping("list/{page}")
    public Page<Article> getAllPosts(@PathVariable(value="page") int page){
        return articleRepository.findAllByOrderByIdDesc(new PageRequest(page,10));
    }

    @GetMapping("list/{userId}/{page}")
    public Page<Article> getAllPosts(
            @PathVariable(value="userId") Long id,
            @PathVariable(value="page") int page){
        User user = userService.findById(id);
//        return articleRepository.getByUserId(user,new PageRequest(page,10, Sort.Direction.ASC, "createdAt"));
        return articleRepository.getByUserId(user,new PageRequest(page,10));
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> commitArticle(@Valid @RequestBody Article article, Errors errors, Principal principal) {
                if(errors.hasFieldErrors()){
                   throw new CustomException(errors.getFieldError().getDefaultMessage(), HttpStatus.LENGTH_REQUIRED);
                }
        String currentUser= principal.getName();
        User user = userService.findByUsername(currentUser);
        article.setAuthor(currentUser);
        article.setUser(user);

        try{
            articleRepository.save(article);
        }
        catch(Exception e) {
            throw new CustomException("Title already exists.", HttpStatus.CONFLICT);
        }
//        Article article = new Article();
//        article.setUser(user);
//        article.setAuthor(currentUser);
//        article.setTitle(articleDto.getTitle());
//        article.setContent(articleDto.getContent());
//        articleRepository.save(article);

        return ResponseEntity.ok(new Message("Article submitted"));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteArticle(@PathVariable(value="id") Long id) {
//                Article article = articleRepository.getOne(id);
                articleRepository.deleteById(id);
                return ResponseEntity.ok(new Message("Deleted"));
    }

//    @PostMapping()
//    public Article createPost(@Valid @RequestBody Article article) {
//        return articleRepository.save(article);
//    }
}
